const express = require('express');
const path = require('path');
const app = express();

const serverPort = 80;
const assetsFolder = path.join(__dirname, '/client/public/assets');
const htmlFile = path.join(__dirname, '/client/public/index.html');


app.use('/assets', express.static(assetsFolder));
app.get('/', (req, res) => {
    res.sendFile(htmlFile);
});

app.listen(serverPort);