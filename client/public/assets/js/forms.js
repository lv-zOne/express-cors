//Return object with form field elements values for the given method
const apiObjectGen = (solution, method) => {
  //e.x. solution = 'XHR', method = 'POST'
  const lcSolution = solution.toLowerCase(), lcMethod = method.toLowerCase();

  return {
    firstName: document.getElementById(`firstname_${lcSolution}_${lcMethod}`).value,
    lastName: document.getElementById(`lastname_${lcSolution}_${lcMethod}`).value,
    email: document.getElementById(`email_${lcSolution}_${lcMethod}`).value,
    message: document.getElementById(`message_${lcSolution}_${lcMethod}`).value,
    hidden: document.getElementById(`hidden-field_${lcSolution}_${lcMethod}`).value,
  };
};

//On DOM Ready
document.addEventListener('DOMContentLoaded', (domLoadedEvent) => {
  //Get form references
  const xhrPostForm = document.getElementById('form_xhr_post');
  const xhrGetForm = document.getElementById('form_xhr_get');
  const fetchPostForm = document.getElementById('form_fetch_post');
  const fetchGetForm = document.getElementById('form_fetch_get');

  //Form submit listeners
  //XMLHttpRequest - POST
  xhrPostForm.addEventListener('submit', (e) => {
    //Get form data upon form submit
    const xhrPostData = apiObjectGen('XHR', 'POST');
    console.log(
      'Attempting to AJAX (XMLHttpRequest) data to server using POST method, data:',xhrPostData
    );

    //Prevent browser's native form submit as we'll use own solution
    e.preventDefault();

    //Prepare new Request
    const oReq = new XMLHttpRequest();

    //Prepare listener for when the request is done
    oReq.addEventListener('readystatechange', (o) => {
      if (o.target.readyState == 4) {
        console.log('Response from server:', JSON.parse(o.target.response));
      }
    });

    //Open Request
    oReq.open(
      xhrPostForm.getAttribute('method').toUpperCase(),
      xhrPostForm.getAttribute('action'),
      true
    );

    //Set Request header - content type json
    oReq.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

    //Send request payload
    oReq.send(JSON.stringify(xhrPostData));
  });

  //XMLHttpRequest - GET
  xhrGetForm.addEventListener('submit', (e) => {
    const xhrGetData = apiObjectGen('XHR', 'GET');
    console.log(
      'Attempting to AJAX (XMLHttpRequest) data to server using GET method, data:', xhrGetData
    );

    //Prevent browser's native form submit as we'll use own solution
    e.preventDefault();

    //Prepare new Request
    const oReq = new XMLHttpRequest();

    //Prepare listener for when the request is done
    oReq.addEventListener('readystatechange', (o) => {
      if (o.target.readyState == 4) {
        console.log('Response from server:', JSON.parse(o.target.response));
      }
    });

    //Prepare JSON data as GET parameters
    const urlParams = Object.keys(xhrGetData)
      .map((k) => {
        return encodeURIComponent(k) + '=' + encodeURIComponent(xhrGetData[k]);
      })
      .join('&');

    //Open Request
    oReq.open(
      xhrGetForm.getAttribute('method').toUpperCase(),
      xhrGetForm.getAttribute('action') + '?' + urlParams,
      true
    );

    //Send request - no payload needed as it's all part of the GET URL, where we've converted all object keys and values to query parameters and values
    oReq.send();
  });

  //Fetch API - POST
  fetchPostForm.addEventListener('submit', (e) => {
    //Get form data upon form submit
    const fetchPostData = apiObjectGen('Fetch', 'POST');
    console.log(
      'Attempting to use Fetch API for sending data to server using POST method, data:', fetchPostData
    );

    //Prevent browser's native form submit as we'll use own solution
    e.preventDefault();

    //Prepare new Request
    const oReq = new Request(fetchPostForm.getAttribute('action'), {
      headers: {
        'Content-Type': 'application/json',
      },
      method: fetchPostForm.getAttribute('method').toUpperCase(),
      body: JSON.stringify(fetchPostData),
    });

    fetch(oReq)
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw new Error('Something went wrong on api server!');
        }
      })
      .then((response) => {
        console.log('Response from server:', response);
      })
      .catch((error) => {
        console.error(error);
      });
  });

  //Fetch API - GET
  fetchGetForm.addEventListener('submit', (e) => {
    const fetchGetData = apiObjectGen('Fetch', 'GET');
    console.log(
      'Attempting to use Fetch API for sending data to server using GET method, data:', fetchGetData
    );

    //Prevent browser's native form submit as we'll use own solution
    e.preventDefault();

    //Prepare JSON data as GET parameters
    const urlParams = Object.keys(fetchGetData)
      .map((k) => {
        return (
          encodeURIComponent(k) + '=' + encodeURIComponent(fetchGetData[k])
        );
      })
      .join('&');

    //Prepare new Request
    const oReq = new Request(
      fetchGetForm.getAttribute('action') + '?' + urlParams,
      { method: fetchGetForm.getAttribute('method').toUpperCase() }
    );

    fetch(oReq)
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw new Error('Something went wrong on api server!');
        }
      })
      .then((response) => {
        console.log('Response from server:', response);
      })
      .catch((error) => {
        console.error(error);
      });
  });
});
