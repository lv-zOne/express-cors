const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();

const serverPort = 3500;
const assetsFolder = path.join(__dirname, '/server/public');
const htmlFile = path.join(__dirname, '/server/views/index.html');

//CORS issue resolve - if you comment the block below, you can reproduce again the CORS issue
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS'); //list any other methods you may use, such as PUT, DELETE, etc
  next();
});

app.use(bodyParser.json());

app.use('/assets', express.static(assetsFolder));

app.get('/', (req, res) => {
  res.sendFile(htmlFile);
});

//API calls - also different based on method
//XHR - GET - extract payload from query parameters
app.get('/api/xhr', (req, res) => {
  res.send({
    serverResponseMethodUsed: 'GET',
    serverResponseDataSentVia: 'XMLHttpRequest / AJAX',
    serverResponseField: 'Value for Response Field',
    serverResponseOriginalPayloadSent: req.query,
  });
});

//XHR - POST - extract payload from request
app.post('/api/xhr', (req, res) => {
  res.send({
    serverResponseMethodUsed: 'POST',
    serverResponseDataSentVia: 'XMLHttpRequest / AJAX',
    serverResponseField: 'Value for Response Field',
    serverResponseOriginalPayloadSent: req.body,
  });
});

//Fetch - GET - extract payload from query parameters
app.get('/api/fetch', (req, res) => {
  res.send({
    serverResponseMethodUsed: 'GET',
    serverResponseDataSentVia: 'Fetch API',
    serverResponseField: 'Value for Response Field',
    serverResponseOriginalPayloadSent: req.query,
  });
});

//Fetch - POST - extract payload from request
app.post('/api/fetch', (req, res) => {
  res.send({
    serverResponseMethodUsed: 'POST',
    serverResponseDataSentVia: 'Fetch API',
    serverResponseField: 'Value for Response Field',
    serverResponseOriginalPayloadSent: req.body
  });
});

app.listen(serverPort);
