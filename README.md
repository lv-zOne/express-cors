# Local development CORS Issues and How to handle them in Express #

This project demonstrates how to solve CORS issues with localhost development as well as demonstrates a number of other cool stack to setup quickly a NodeJS server using express, server & client, route processing, static data handling.

![](doc-images/cors.png?raw=true)

### What is this repository for? ###

* This repo demonstrates a common problem web developers may face, having both server and client on the same localhost (different ports) - fetch calls hit CORS - [Cross-origin resource sharing](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) related problems and Chrome refusung to see those calls through.
* I am using [Express](https://expressjs.com) (as NodeJS server solution for handling requests/routes) and have written a simple client app with few forms going to different server routes.
* Of course I am providing a simple solution for the CORS problem, by setting up response headers
* In addition to addressing the CORS problem, this project also demonstrates a quick way to have **NodeJS based simple server** and **client** solution using Express (also serving static images, css from assets folder declared as public). I am also using nodemon during development so that it watches for my server and client apps changes and automatically restarts node running them for me. If you haven't used [nodemon](https://nodemon.io/) before I highly recomment it.
* The server itself is supporting a client view of the root page - `http://localhost:3500/` - where it serves HTML, CSS and images from a static folder. POST & GET calls are handled via various `/api/` routes.

### How do I get set up? ###

* Checkout
* Run `npm install` to resolve dependencies
* Run `npm run server` to run the **server** that would handle CORS requests on `http://localhost:3500` (keep that running in one Terminal tab)
* Run `npm run client` to run a simple **client** (still placed in a NodeJS server) on `http://localhost(:80)` with multiple forms sending data via POST/GET to different routes on the server (run this in a new Terminal tab)
* OR run `npm run both` to run **both server and client** (run this if on Linux based env, otherwise use packages like `npm-run-all` or `concurrently`) with common console output to the same/single terminal/tab
* Keep an eye on console outputs for Server and Client.
* You may have to comment a section of the server code and rebuild to see the CORS issues in action, to then enable the code again to resolve the CORS issues

## Note - it may look like there is a lot of common code in server.js and client.js ##

... BUT ... remember that the idea is that they are both standalone and separate servers, doing different things - one is the Back End (server.js) and the other is the Front End (client.js).
The common part is that they both run on NodeJS servers on different ports, both using Express to render HTML and static assets where needed, and they differ in the fact that the Server handles all the `/api/` routes that the Client forms send data to (via POST/GET). In theory, I could get all the files from `client/public` folder and place them anywhere else, or on a different server - we do place them on the same server here to highlight the CORS issues.

## Note - using NodeJS to serve (listen) to port 80 on Linux issue ##

By default Linux (Ubuntu etc) won't allow NodeJS app to listen to port 80 (even if Apache is not present/killed/stopped) - it's not a matter of port conflict, it's a permission issue

A [quick fix](https://stackoverflow.com/questions/16573668/best-practices-when-running-node-js-with-port-80-ubuntu-linode) is to run something like this (below is for Ubuntu/Debian, but I'm sure you can find similar solution for other Linux OS-es )

```
sudo apt-get install libcap2-bin
sudo setcap cap_net_bind_service=+ep `readlink -f \`which node\``
```

#### Client Console & Network Tabs View - CORS issue ####

When attempting to send data to a different server (same server but different port, which is classed as different server) we get the following CORS issue in the browser:

##### XMLHttpRequest (AJAX) / Fetch API - POST & GET methods #####

XMLHttpRequest:

![](doc-images/CORS-XHR-console.png?raw=true)

Fetch API:

![](doc-images/CORS-Fetch-console.png?raw=true)

The network call never succeeds (or fails) on the server (hence no 400, 200 etc responses can be seen in the network tab for those two calls - GET and POST) due to the CORS issue.

XMLHttpRequest:

![](doc-images/CORS-XHR-network.png?raw=true)

Fetch API:

![](doc-images/CORS-Fetch-network.png?raw=true)

Now adding this to the server.js code, **solves the CORS issues** altogether:

```
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS'); //list any other methods you may use, such as PUT, DELETE, etc
  next();
});
```

You can see the **calls are now successful**, both POST and GET, and we manage to get a response from the server, which includes the original payloads (or query parameters in the case of the GET call), and added new data from the server too. So now we have it end to end - from the client form, to the server, and back to the client where the response from the server is output in the console.

XMLHttpRequest:

![](doc-images/CORS-solved-XHR-console.png?raw=true)

Fetch API:

![](doc-images/CORS-solved-Fetch-console.png?raw=true)

Here are the requests made using either **XMLHttpRequest (AJAX) for POST and GET** or **Fetch API for POST and GET**, and their corresponding server responses as seen in Network tab.

XMLHttpRequest POST:

![](doc-images/CORS-XHR-Post-network-payload.png?raw=true)

![](doc-images/CORS-XHR-Post-network-response.png?raw=true)

XMLHttpRequest GET:

![](doc-images/CORS-XHR-Get-network-query-params.png?raw=true)

![](doc-images/CORS-XHR-Get-network-response.png?raw=true)

Fetch API POST:

![](doc-images/CORS-Fetch-Post-network-payload.png?raw=true)

![](doc-images/CORS-Fetch-Post-network-response.png?raw=true)

Fetch API GET:

![](doc-images/CORS-Fetch-Get-network-query-params.png?raw=true)

![](doc-images/CORS-Fetch-Get-network-response.png?raw=true)

